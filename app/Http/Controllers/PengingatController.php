<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class PengingatController extends Controller
{
    public function create(){
        $pelatihan = DB::table('pelatihan')->get();
        return view('pengingat.create',compact('pelatihan'));
    }

    public function store(Request $request){
        $request->validate([
            'catatan' => 'required',
            'jadwal_senam' => 'required',
            'pelatihan_id' => 'required',
        ]);

        DB::table('pengingat')->insert([
            'catatan' => $request['catatan'],
            'jadwal_senam' => $request['jadwal_senam'],
            'pelatihan_id' => $request['pelatihan_id'],
        ]);

        return redirect('/pengingat');
    }

    public function index(){
        $pengingat = DB::table('pengingat')->get();
        return view('pengingat.index',compact('pengingat'));
    }

    public function show($id){
        $pengingat = DB::table('pengingat')->where('id',$id)->first();
        return view('pengingat.show',compact('pengingat'));
    }

    public function edit($id){
        $pengingat = DB::table('pengingat')->where('id',$id)->first();
        $pelatihan = DB::table('pelatihan')->get();
        return view('pengingat.create',compact('pelatihan'));
        return view('pengingat.edit',compact('pengingat'));
    }

    public function update($id, Request $request){
        $request->validate([
            'catatan' => 'required',
            'jadwal_senam' => 'required',
            'pelatihan_id' => 'required',
        ]);

    $query = DB::table('pengingat')
                ->where('id',$id)
                ->update([
                    'catatan' => $request['catatan'],
                    'jadwal_senam' => $request['jadwal_senam'],
                    'pelatihan_id' => $request['pelatihan_id'],
                ]);
        
        return redirect('/pelatihan');
    }

    public function destroy($id){
        DB::table('pengingat')->where('id',$id)->delete();

        return redirect('/pengingat');
    }

}