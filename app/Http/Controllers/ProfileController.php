<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Profile;

class ProfileController extends Controller
{
    public function create(){
        $user = DB::table('user')->get();
        return view('profile.create',compact('user'));
    }

    public function store(Request $request){
        $request->validate([
            'umur' => 'required',
            'biodata' => 'required',
            'user_id' => 'required',
        ]);

        DB::table('profile')->insert([
            'umur' => $request['umur'],
            'biodata' => $request['biodata'],
            'user_id' => $request['user_id'],
        ]);

        return redirect('/profile/create');
    }
    public function index(){
        $profile = DB::table('profile') ->get();
        return view('profile.index', compact('profile'));
    }

    public function show($id){
        $profile = Profile::findOrFail($id);
        return view('profile.show',compact('profile'));
    }
    
}

