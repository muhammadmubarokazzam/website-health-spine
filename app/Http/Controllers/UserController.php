<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function create(){
        return view('user.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama_lengkap' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);

        DB::table('user')->insert([
            'nama_lengkap' => $request['nama_lengkap'],
            'email' => $request['email'],
            'password' => $request['password'],
        ]);

        return redirect('/user/create');
    }

    public function index(){
        $user = DB::table('user')->get();
        return view('user.index', compact('user'));
    }

    public function show($id){
        $user = DB::table('user')->where('id', $id)->first();
        return view('user.show',compact('user'));
    }
}

