<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use App\Pelatihan;
use File;
use RealRashid\SweetAlert\Facades\Alert;

class PelatihanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pelatihan = Pelatihan::get();
        return view('pelatihan.index',compact('pelatihan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = DB::table('user')->get();
        return view('pelatihan.create',compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'senam_sederhana' => 'required',
            'video_senam' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'user_id' => 'required',
        ]);

        $video_senamName = time().'.'.$request->video_senam->extension();

        $request->video_senam->move(public_path('gambar'),$video_senamName);

        $pelatihan = new Pelatihan;

        $pelatihan->senam_sederhana = $request->senam_sederhana;
        $pelatihan->video_senam = $video_senamName;
        $pelatihan->user_id = $request->user_id;

        $pelatihan->save();

        //Alert::success('Berhasil !', 'Tambah Data Film Berhasil');
        return redirect('/pelatihan/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pelatihan = Pelatihan::findOrFail($id);

        return view('pelatihan.show',compact('pelatihan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = DB::table('user')->get();
        $pelatihan = Pelatihan::findOrFail($id);

        return view('pelatihan.edit',compact('pelatihan','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'senam_sederhana' => 'required',
            'video_senam' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'user_id' => 'required',
        ]);

        $pelatihan = Pelatihan::find($id);

        if($request->has('video_senam')){
            $video_senamName = time().'.'.$request->video_senam->extension();
            $request->video_senam->move(public_path('gambar'),$video_senamName);

            $pelatihan->senam_sederhana = $request->senam_sederhana;
            $pelatihan->video_senam = $video_senamName;
            $pelatihan->user_id = $request->user_id;

        }else{
            $pelatihan->senam_sederhana = $request->senam_sederhana;
            $pelatihan->user_id = $request->user_id;
        }

        $pelatihan->update();
        //Alert::success('Update','Data Film Berhasil Diupdate !');
        return redirect('/pelatihan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pelatihan = Pelatihan::find($id);

        $path = "gambar/";
        File::delete($path.$pelatihan->video_senam);
        $pelatihan->delete();
        //Alert::success('Delete','Data Film Berhasil Dihapus !');
        return redirect('/pelatihan');
    }
}
