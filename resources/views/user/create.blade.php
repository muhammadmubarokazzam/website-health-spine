@extends('layout.master')

@section('judul')
Halaman User
@endsection

@section('content')

<form action="/user" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama Lengkap</label>
      <input type="text" name="nama_lengkap" class="form-control">
    </div>
    @error('nama_lengkap')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Email</label>
      <input type="text" name="email" class="form-control">
    </div>
    @error('email')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Password</label>
      <input type="text" name="password" class="form-control">
    </div>
    @error('password')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>




@endsection