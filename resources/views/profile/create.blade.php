@extends('layout.master')

@section('judul')
Halaman Profile
@endsection

@section('content')

<form action="/profile" method="POST">
    @csrf
    <div class="form-group">
      <label>Umur</label>
      <input type="integer" name="umur" class="form-control">
    </div>
    @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Biodata</label>
      <textarea name="biodata" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('biodata')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>User Id</label> <br>
      <select name="user_id" class="js-example-basic-single" style="width: 100% " id="">
        <option value="">---pilih User---</option>

        @foreach ($user as $item)
            <option value="{{$item->id}}">{{$item->nama_lengkap}}</option>
        @endforeach
      </select>
    </div>
    @error('user_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>




@endsection