@extends('layout.sensei')

@section('judul')
Halaman List Pelatihan
@endsection

@section('judul1')
Program Utama Olahraga
@endsection

@section('content')

<a href="/pelatihan/create" class="btn btn-primary my-2">Tambah</a>

<div class="row">
  @forelse ($pelatihan as $item)
    <div class="col-4">
      <div class="card">
        <img src="{{asset('gambar/'. $item->video_senam)}}" class="card-img-top" alt="...">
        <div class="card-body">
          <h5>{{$item->id}}</h5>
          <p class="card-text">{{Str::limit($item->senam_sederhana, 30)}}</p>
          <form action="/pelatihan/{{$item->id}}" method="POST">
              @csrf
              @method('DELETE')
              <a href="/pelatihan/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
              <a href="/pelatihan/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
              <input type="submit" value="Delete" class="btn btn-danger btn-sm">
          </form>
        </div>
      </div>
    </div>

  @empty
      <h4>Data Kosong</h4>
  @endforelse
</div>

  


@endsection
