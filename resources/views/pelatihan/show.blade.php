@extends('layout.sensei')

@section('judul')
Halaman Detail Pelatihan {{$pelatihan->id}}
@endsection

@section('judul1')
Program
@endsection

@section('content')

<img src="{{asset('gambar/'. $pelatihan->video_senam)}}" alt="">
<h1>{{$pelatihan->senam_sederhana}}</h1>

<a href="/pelatihan" class="btn btn-secondary">Kembali</a>

@endsection