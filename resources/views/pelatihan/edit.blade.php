@extends('layout.sensei')

@section('judul')
Halaman Edit Pelatihan
@endsection

@section('judul1')
Form Isi
@endsection

@section('content')
    <form action="/pelatihan/{{$pelatihan->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>Senam Sederhana</label>
            <textarea name="senam_sederhana" class="form-control" id="" cols="30" rows="10">{{$pelatihan->senam_sederhana}}</textarea>
        </div>
        @error('pelatihan')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label> Video Senam </label>
            <input type="file" name="video_senam" class="form-control">
        </div>
        @error('video_senam')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
        <label>User</label> <br>
            <select name="user_id" class="form-control" id="">
                <option value="">---pilih User---</option>

                @foreach ($user as $item)
                    @if ($item->id === $pelatihan->user_id)

                        <option value="{{$item->id}}" selected>{{$item->nama_lengkap}}</option>
                    
                    @else

                        <option value="{{$item->id}}">{{$item->nama_lengkap}}</option>

                    @endif
                @endforeach
            </select>
        </div>
        @error('pelatihan_id')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>



@endsection           