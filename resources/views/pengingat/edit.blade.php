@extends('layout.sensei')

@section('judul')
Halaman Edit {{$pengingat->jadwal_senam}}
@endsection

@section('judul1')
Edit
@endsection

@section('content')
    <form action="/pengingat/{{$pengingat->id}}" method="POST">
      @csrf
      @method('PUT')
        <div class="form-group">
            <label> Catatan </label>
            <textarea name="catatan" class="form-control" value="{{$pengingat->jadwal_senam}}" id="" cols="30" rows="10"></textarea>
        </div>
        @error('catatan')
        <div class='alert alert-danger'>{{ $message }}</div>
        @enderror

        <div class='form-group'>
            <label> Jadwal Senam </label>
            <input type='text' name='jadwal_senam' value='{{$pengingat->jadwal_senam}}' class='form-control'>
        </div>
        @error('jadwal_senam')
        <div class='alert alert-danger'>{{ $message }}</div>
        @enderror

        <div class="form-group">
          <label>Pelatihan Id</label> <br>
          <select name="pelatihan_id" class="js-example-basic-single" style="width: 100% " >
              <option value="">---pilih Pelatihan---</option>
  
              @foreach ($pelatihan as $item)
                  <option value="{{$item->id}}">{{$item->senam_sederhana}}</option>
              @endforeach
          </select>
          </div>
          @error('pelatihan_id')
          <div class="alert alert-danger">{{ $message }}</div>
          @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection   