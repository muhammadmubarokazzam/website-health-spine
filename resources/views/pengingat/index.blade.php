@extends('layout.master')

@section('judul')
Halaman Pengingat
@endsection

@section('judul1')
Pengingat Jadwal
@endsection

@section('content')
<a href="/pengingat/create" class="btn btn-success mb-3">Tambah Data</a>

<table class="table">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Catatan</th>
            <th scope="col">Jadwal Senam</th>
            <th scope="col">Pelatihan_ID</th>
            <th scope="col">Action</th>
        </tr>
    </thead>

    <tbody>
        @forelse ($pengingat as $key=>$item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->catatan}}</td>
                <td>{{$item->jadwal_senam}}</td>
                <td>{{$item->pelatihan_id}}</td>
                <td>
                    <form action="/pengingat/{{$item->id}}" method="POST">
                      <a href="/pengingat/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                      <a href="/pengingat/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                      @method('delete')
                      @csrf
                      <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
        <tr>
            <td>Data Masih Kosong</td>
        </tr>
        @endforelse
    </tbody>
</table>
@endsection

@section('judul2')
Persen Pengingat
@endsection


@section('content1')
    <!--<a href="/edukasi">Kembali</a>-->
    <table class="table table-bordered">
        <thead>                  
          <tr>
            <th style="width: 10px">#</th>
            <th>Task</th>
            <th>Kebutuhan</th>
            <th style="width: 40px">Persen</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1.</td>
            <td>Hands-Up selama 15 detik</td>
            <td>
              <div class="progress progress-xs progress-striped active">
                <div class="progress-bar bg-success" style="width: 90%"></div>
              </div>
            </td>
            <td><span class="badge bg-success">90%</span></td>
          </tr>

          <tr>
            <td>2.</td>
            <td>Hands Backwards selama 15 detik</td>
            <td>
              <div class="progress progress-xs progress-striped active">
                <div class="progress-bar bg-success" style="width: 85%"></div>
              </div>
            </td>
            <td><span class="badge bg-success">85%</span></td>
          </tr>

          <tr>
            <td>3.</td>
            <td>Hands-Down selama 15 Detik</td>
            <td>
              <div class="progress progress-xs progress-striped active">
                <div class="progress-bar bg-primary" style="width: 80%"></div>
              </div>
            </td>
            <td><span class="badge bg-primary">80%</span></td>
          </tr>

          <tr>
            <td>4.</td>
            <td>Cobra Pose selama 15 detik</td>
            <td>
              <div class="progress progress-xs progress-striped active">
                <div class="progress-bar bg-primary" style="width: 75%"></div>
              </div>
            </td>
            <td><span class="badge bg-primary">75%</span></td>
          </tr>

          <tr>
            <td>5.</td>
            <td>V-Pose selama 15 Detik</td>
            <td>
              <div class="progress progress-xs progress-striped active">
                <div class="progress-bar bg-primary" style="width: 70%"></div>
              </div>
            </td>
            <td><span class="badge bg-primary">70%</span></td>
          </tr>

          <tr>
            <td>6.</td>
            <td>Child Pose selama 15 Detik</td>
            <td>
              <div class="progress progress-xs">
                <div class="progress-bar bg-warning" style="width: 55%"></div>
              </div>
            </td>
            <td><span class="badge bg-warning">55%</span></td>
          </tr>

          <tr>
            <td>7.</td>
            <td>Right Lunge selama 15 Detik</td>
            <td>
              <div class="progress progress-xs">
                <div class="progress-bar bg-warning" style="width: 50%"></div>
              </div>
            </td>
            <td><span class="badge bg-warning">50%</span></td>
          </tr>

          <tr>
            <td>8.</td>
            <td>Left Lunge selama 15 Detik</td>
            <td>
              <div class="progress progress-xs">
                <div class="progress-bar bg-danger" style="width: 40%"></div>
              </div>
            </td>
            <td><span class="badge bg-danger">40%</span></td>
          </tr>

        </tbody>
      </table>
@endsection