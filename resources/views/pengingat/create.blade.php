@extends('layout.sensei')

@section('judul')
Form Pengingat
@endsection

@section('judul1')
Isi
@endsection

@section('content')
    <form action="/pengingat" method="POST">
      @csrf
        <div class="form-group">
            <label>Catatan</label>
            <textarea name="catatan" class="form-control" id="" cols="30" rows="10"></textarea>
        </div>
        @error('pengingat')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label> Jadwal Senam </label>
            <input type="text" name="jadwal_senam" class="form-control">
        </div>
        @error('jadwal_senam')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
        <label>Pelatihan Id</label> <br>
        <select name="pelatihan_id" class="js-example-basic-single" style="width: 100% " >
            <option value="">---pilih Pelatihan---</option>

            @foreach ($pelatihan as $item)
                <option value="{{$item->id}}">{{$item->senam_sederhana}}</option>
            @endforeach
        </select>
        </div>
        @error('pelatihan_id')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection      