@extends('layout.master')

@section('judul')
Halaman Rekomendasi
@endsection

@section('judul1')
Makanan/Minuman Rekomendasi
@endsection

@push('script')
<script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')
    <div class="card">
        <div class="card-header">
          <h5 class="card-title">Mengkonsumsi makanan berkalsium sangat dianjurkan juga bagi wanita berusia di atas 50 tahun dan setiap orang di atas umur 70 tahun harus mendapatkan 1.200 mg per hari, sementara kebutuhan anak-anak berusia 4-18 tahun disarankan untuk mendapatkan asupan kalsium 1.300 mg.</h5>
        </div>

        <!-- /.card-header -->
        <div class="card-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Daftar Makanan Yang Mengandung Kalsium Tinggi</th>
              <th>Manfaat</th>
              <th>Kelebihan</th>
              <th>Takaran</th>
            </tr>
            </thead>

            <tbody>
            <tr>
              <td>Biji-bijian</td>
              <td>Biji-bijian adalah sumber tenaga nutrisi. Beberapa di antaranya mengandung kalsium tinggi, termasuk biji wijen, seledri dan biji chia.</td>
              <td>mengandung kalsium tinggi</td>
              <td>1 sendok makan (15 gram)</td>
            </tr>

            <tr>
              <td>Yogurt</td>
              <td>mengonsumsi yogurt dengan kualitas makanan yang lebih baik secara keseluruhandapat meningkatkan kesehatan metabolik. Para pelaku yang mengonsumsi yoghurt memiliki risiko penyakitmetabolik yang lebih rendah, seperti penyakit diabetes tipe 2 dan penyakit jantung.</td>
              <td>kaya akan bakteri probiotik hidup,yang memiliki berbagai manfaat bagi kesehatan</td>
              <td>satu cangkir</td>
            </tr>

            <tr>
              <td>Keju</td>
              <td>kalsium dalam produk olahan susu ini lebih mudah diserap oleh tubuh Anda daripada mengkonsumsi makanan dari bahan lain</td>
              <td>sumber makanan berkalsium tinggi</td>
              <td>15 gram</td>
            </tr>

            <tr>
              <td>Sarden dan Salmon kaleng</td>
              <td>menyediakan protein berkualitas tinggi dan asam lemak omega-3 yang bagus untuk jantung, otak dan kesehatan kulit Anda.</td>
              <td>kandungan kalsium yang tinggi</td>
              <td>92 gram</td>
            </tr>

            <tr>
              <td>Kacang dan Lentil (kacang lunak)</td>
              <td>dapat membantu menurunkan kadar kolesterol</td>
              <td>salah satu sumber kalsium yang baik</td>
              <td>Satu cangkir</td>
            </tr>

            <tr>
              <td>Almond</td>
              <td>dapat membantu menurunkan tekanan darah, lemak tubuh dan faktor risiko lainnyaterutama penyakit metabolik.</td>
              <td>mengandung kalsium paling tinggi</td>
              <td>Satu cangkir</td>
            </tr>

            <tr>
              <td>Whey Protein</td>
              <td> penurunan berat badan dan memperbaiki kontrol gula darah</td>
              <td> sumber protein yang sangat baik dan dan kaya asam amino yang lebih cepat dicerna dalam usus halus organ pencernaan</td>
              <td> Satu sendok makan</td>
            </tr>

            <tr>
              <td>sayuran hijau</td>
              <td>baik untuk tubuh tersebut meliputi sayuran collard, bayam dan kangkung.</td>
              <td>mengandung kalsium tinggi</td>
              <td>Satu cangkir</td>
            </tr>

            <tr>
              <td>Rhubarb (semacam pohon keladi atau talas)</td>
              <td>mengandung serat prebiotik, yang dapat meningkatkan bakteri sehat di usus Anda</td>
              <td>memiliki banyak serat, vitamin K, kalsium dan sejumlah kecil vitamin dan mineral lainnya.</td>
              <td>Satu cangkir</td>
            </tr>

            <tr>
              <td>Makanan penambah asupan kalsium</td>
              <td>sereal, tepung jagung, dan susu</td>
              <td>tubuh Anda tidak dapat menyerap semua kalsium sekaligus dan sebaiknya Andabagi asupan makanan berkalsium tersebut setiap harinya</td>
              <td>Satu cangkir</td>
            </tr>

            <tr>
              <td>Amaranth (sejenis sayur – sayuran, salah satunya bayam)</td>
              <td>sumber folat yang baik dan sangat tinggi untuk jenis mineral tertentu,termasuk mangan, magnesium, fosfor dan besi.</td>
              <td>merupakan makanan yang sangat bergizi dan kaya kalsium</td>
              <td>Satu cangkir</td>
            </tr>

            <tr>
              <td>Edamame (kedelai) dan tahu</td>
              <td>sumber protein yang baik dan memberikan kebutuhan folat harian Anda dalam satu porsi</td>
              <td>jumlah kalsium yaitu 30% RDI</td>
              <td>Satu cangkir</td>
            </tr>

            <tr>
              <td>Minuman yang difortifikasi</td>
              <td>asupan kalsium dari minuman non-susu, seperti susu kedelai</td>
              <td>susu kedelai menjadiasupan non-susu yang paling bergizi seperti yang terkandung dalam susu sapi</td>
              <td>Satu cangkir</td>
            </tr>

            <tr>
              <td>buah Ara kering</td>
              <td>memenuhi kebutuhan mineral kalsium harian Anda</td>
              <td>terkandung banyak kalsiumdaripada buah kering lainnya</td>
              <td>28 gram</td>
            </tr>

            <tr>
              <td>Susu</td>
              <td>minuman bergizi dengan sumber kalsium terbaik dan efektif untuk menambah kebutuhan asupan kalsium bagi tubuh</td>
              <td>kandungan kalsium yang tinggi, susu merupakan sumber protein, vitamin A dan vitamin D yang sangatbaik bagi tubuh</td>
              <td>Satu cangkir</td>
            </tr>

            
            </tbody>
            
            <tfoot>
            <tr>
              <th>Daftar Makanan Yang Mengandung Kalsium Tinggi</th>
              <th>Manfaat</th>
              <th>Kelebihan</th>
              <th>Takaran</th>
            </tr>
            </tfoot>

            
            

          </table>
        </div>
  </div>
@endsection

@section('judul2')
Gambar Makanan/Minuman
@endsection

@section('content1')
  <div class="row">
    <img src="{{asset('admin/dist/img/Manfaat_Makan_Lebih_Banyak_Buah_dan_Sayuran_xmjed0.jpg')}}" class="card-img-top" alt="..." style="opacity: .8">
  </div>
  <br>
  <div class="row">
    <img src="{{asset('admin/dist/img/Biji-bijian.jpg')}}" class="card-img-top" alt="..." style="opacity: .8">
  </div>
  <br>
  <div class="row">
    <img src="{{asset('admin/dist/img/foto-susu-ragi-alternatif-susu-sapi-bagi-para-vegetarian.jpg')}}" class="card-img-top" alt="..." style="opacity: .8">
  </div>
@endsection