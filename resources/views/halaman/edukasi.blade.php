@extends('layout.master')

@section('judul')
Halaman Edukasi
@endsection

@section('judul1')
Gambar Penyakit Tulang Belakang
@endsection

@push('script')
<script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')

<div class="row">
  <img src="{{asset('admin/dist/img/tulang belakang32s.jpg')}}" class="card-img-top" alt="..." style="opacity: .8">
</div>

@endsection


@section('judul2')
Titik Penyakit Tulang Belakang
@endsection

@section('content1')

<div class="card">
  <div class="card-header">
    <h3 class="card-title">Hubungan Ruas Tulang Belakang dengan Penyakit</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <table id="example1" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>Nomor</th>
        <th>Keterangan</th>
      </tr>
      </thead>

      <tbody>
      <tr>
        <td>C1</td>
        <td>Ketombe, Sakit Kepala, gugup, sulit tidur, tekanan darah tinggi</td>
      </tr>

      <tr>
        <td>C2</td>
        <td>Sinusitis, tuli, sakit telinga, kebutaan</td>
      </tr>

      <tr>
        <td>C3</td>
        <td>jerawat, keracunan, radang urat syaraf, sakit syaraf</td>
      </tr>

      <tr>
        <td>C4</td>
        <td>Alergi (demam tinggi), katarak</td>
      </tr>

      <tr>
        <td>C5</td>
        <td>Serak, radang tenggorokan, sakit tenggorokan</td>
      </tr>

      <tr>
        <td>C6</td>
        <td>Leher kaku, amandel, batuk, sesak nafas, pegal di lengan</td>
      </tr>

      <tr>
        <td>C7</td>
        <td>Radang kandung lendir, gondok, masuk angin</td>
      </tr>

      <tr>
        <td>C8</td>
        <td>Pegal di tangan, batuk, asma</td>
      </tr>

      <tr>
        <td>T1</td>
        <td>Masalah jantung, sakit di dada</td>
      </tr>

      <tr>
        <td>T2</td>
        <td>Radang selaput dada, radang paru-paru, flu, radang cabang tenggorokan</td>
      </tr>

      <tr>
        <td>T3</td>
        <td>Masalah empedu, sakit kuning</td>
      </tr>

      <tr>
        <td>T4</td>
        <td>Masalah hati, kurang darah, radang sendi, tekanan darah rendah</td>
      </tr>

      <tr>
        <td>T5</td>
        <td>Sakit perut, gangguan pencernaan</td>
      </tr>

      <tr>
        <td>T6</td>
        <td>Sariawan, radang perut, kencing manis</td>
      </tr>

      <tr>
        <td>T7</td>
        <td>Sakit perut, kecegukan</td>
      </tr>

      <tr>
        <td>T8</td>
        <td>Alergi (bintik-bintik merah, dsb)</td>
      </tr>

      <tr>
        <td>T9</td>
        <td>Pengerasan pembuluh nadi, masalah ginjal, kelelahan</td>
      </tr>

      <tr>
        <td>T10</td>
        <td>Jerawat, bisul, keracunan</td>
      </tr>

      <tr>
        <td>T11</td>
        <td>Kembung, kemandulan, rematik</td>
      </tr>

      <tr>
        <td>T12</td>
        <td>Radang usus besar, sembelit, diare, turun berok</td>
      </tr>

      <tr>
        <td>L1</td>
        <td>Kram perut, asam lambung, radang usus buntu, pembuluh mekar</td>
      </tr>

      <tr>
        <td>L2</td>
        <td>Penyakit kelamin, masalah kandung kemih, sakit lutut</td>
      </tr>

      <tr>
        <td>L3</td>
        <td>Pegal pada pinggang, encok</td>
      </tr>

      <tr>
        <td>L4</td>
        <td>Pembengkakan sirkulasi buruk, rasa dingin pada paha dan kaki</td>
      </tr>

      <tr>
        <td>L5</td>
        <td>Kelainan sumsum tulang belakang</td>
      </tr>

      <tr>
        <td>S1 - S5</td>
        <td>Sambungan antara selangkangan dengan tulang ekor</td>
      </tr>

      <tr>
        <td>TE</td>
        <td>Wasir, gatal-gatal, nyeri tulang ekor</td>
      </tr>
      </tbody>
      
      <tfoot>
      <tr>
        <th>Nomor</th>
        <th>Keterangan</th>
      </tr>
      </tfoot>

      

    </table>
  </div>
</div>

@endsection