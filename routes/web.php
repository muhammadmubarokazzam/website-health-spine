<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
//use App\Http\Controllers\UserController;

Route::get('/','WelcomeController@welcome');

// CRUD Profile
Route::resource('profile','ProfileController');
Route::get('/profile/create','ProfileController@create');
Route::post('/profile','ProfileController@store');

// CRUD User
Route::resource('user','UserController');
Route::get('/user/create','UserController@create');
Route::post('/user','UserController@store');


// CRUD Edukasi
Route::get('/edukasi','EdukasiController@edukasi');

Route::group(['middleware' => ['auth']], function (){
    // CRUD Rekomendasi
    Route::get('/rekomendasi','RekomendasiController@rekomendasi');

    // CRUD Pengingat
    Route::get('pengingat/create','PengingatController@create');
    Route::post('/pengingat','PengingatController@store');
    Route::get('/pengingat','PengingatController@index');
    Route::get('/pengingat/{pengingat_id}','PengingatController@show');
    Route::get('/pengingat/{pengingat_id}/edit','PengingatController@edit');
    Route::put('/pengingat/{pengingat_id}','PengingatController@update');
    Route::delete('/pengingat/{pengingat_id}','PengingatController@destroy');

    // CRUD Pelatihan
    Route::resource('pelatihan','PelatihanController');
});

Auth::routes();


